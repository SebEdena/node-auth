const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../model/user");
const config = require("../config");

(async () => {
    try {
        await mongoose.connect(config.DB_HOST, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log("Connected to mongodb");
    } catch (error) {
        console.error("Error! " + error);
    }
})();

exports.register = async (req, res) => {
    //Hash password
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    // Create an user object
    let user = new User({
        email: req.body.email,
        name: req.body.name,
        password: hashPassword,
        role: req.body.role,
    });

    // Save User in the database
    user.save((err, registeredUser) => {
        if (err) {
            console.log(err);
        } else {
            // create payload then Generate an access token
            let payload = { id: registeredUser._id, role: req.body.role || "user" };
            const token = jwt.sign(payload, config.TOKEN_SECRET);
            res.status(200).send({ token });
        }
    });
};

exports.login = async (req, res) => {
    User.findOne({ email: req.body.email }, async (err, user) => {
        if (err) {
            console.log(err);
        } else {
            if (user) {
                const validPass = await bcrypt.compare(req.body.password, user.password);
                if (!validPass) return res.status(401).send("Mobile/Email or Password is wrong");

                // Create and assign token
                let tokenPayload = { id: user._id, role: user.role };
                const token = jwt.sign(tokenPayload, config.TOKEN_SECRET, { expiresIn: "1 minute" });

                const safeUser = { email: user.email, name: user.name, role: user.role };
                const payload = { token: token, user: safeUser };

                res.status(200).send(payload);
            } else {
                res.status(401).send("Invalid mobile");
            }
        }
    });
};

// Access auth users only
exports.userEvent = (req, res) => {
    let events = [
        {
            _id: "1",
            name: "Auto Expo",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
        {
            _id: "2",
            name: "Auto Expo",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
        {
            _id: "3",
            name: "Auto Expo",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
    ];
    res.json(events);
};

exports.adminEvent = (req, res) => {
    let specialEvents = [
        {
            _id: "1",
            name: "Auto Expo Special",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
        {
            _id: "2",
            name: "Auto Expo Special",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
        {
            _id: "3",
            name: "Auto Expo Special",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
        {
            _id: "4",
            name: "Auto Expo Special",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
        {
            _id: "5",
            name: "Auto Expo Special",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
        {
            _id: "6",
            name: "Auto Expo Special",
            description: "lorem ipsum",
            date: "2012-04-23T18:25:43.511Z",
        },
    ];
    res.json(specialEvents);
};
