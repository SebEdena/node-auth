const router = require("express").Router();
const { body, validationResult } = require("express-validator");
const userController = require("../controller/user");
const { hasRole, verifyUserToken } = require("../middleware/auth");
const { ROLE_ADMIN, ROLE_USER } = require("../model/roles");

// Register a new User
router.post(
    "/register",
    [
        body("email").notEmpty().isEmail().normalizeEmail(),
        body("name").notEmpty(),
        body("password").notEmpty(),
        body("user-type_id").notEmpty().isInt(),
    ],
    userController.register
);

// Login
router.post(
    "/login",
    [body("email").notEmpty().isEmail().normalizeEmail(), body("password").notEmpty()],
    userController.login
);

// Auth user only
router.get("/events", verifyUserToken, hasRole(ROLE_USER), userController.userEvent);

// Auth Admin only
router.get("/special", verifyUserToken, hasRole(ROLE_ADMIN), userController.adminEvent);

module.exports = router;
