const mongoose = require("mongoose");
const { ROLE_ADMIN, ROLE_USER } = require("./roles");

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: String,
    password: String,
    name: String,
    role: {
        type: String,
        enum: [ROLE_ADMIN, ROLE_USER],
        default: ROLE_USER,
    },
});

module.exports = mongoose.model("user", userSchema, "users");
